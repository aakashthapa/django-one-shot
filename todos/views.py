from django.shortcuts import get_object_or_404, render, redirect
from .forms import TodoListForm, TodoItemForm
from .models import TodoItem, TodoList

# Create your views here.
def show_TodoList(request):
    todos = TodoList.objects.all()
    context = {
        'todo_list': todos,
        'todo_item': TodoItem(),
    }
    return render(request, "todos/list.html", context)


def show_TodoItem(request, id):
    todoitem = get_object_or_404(TodoList, id=id)
    tasks = todoitem.items.all()
    context = {
        "todoitem": todoitem,
        "tasks": tasks,
    }
    return render(request, "todos/detail.html", context)

def create_TodoList(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save(False)
            list.save()
            return redirect("todo_list_detail", id=list.id) 
    else:
        form =TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)

def edit_TodoList(request, id):
    todo_list = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)

    context = {
        'list': todo_list,
        'form': form,
    }
    return render(request, "todos/edit.html", context)

def delete_TodoList(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")
    context = {
        "todoitem": todo_list,
    }
   
    return render(request, "todos/delete.html", context)

def create_TodoItem(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save(commit=False)
            todo_item.list = TodoList.objects.first()
            todo_item.save()
            return redirect("todo_list_detail", id=todo_item.list.id) 
    else:
        form =TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/itemscreate.html", context)

def update_TodoItem(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)

    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            todo_item= form.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = TodoItemForm(instance=todo_item)

    context = {
        'todo_item': todo_item,
        'form': form,
    }
    return render(request, "todos/updateitem.html", context)