from django.urls import path
from todos.views import show_TodoList, show_TodoItem, create_TodoList, edit_TodoList, delete_TodoList, create_TodoItem, update_TodoItem

urlpatterns = [
    path("", show_TodoList, name= "todo_list_list"),
    path("<int:id>/", show_TodoItem, name= "todo_list_detail"),
    path('create/', create_TodoList, name="todo_list_create"),
    path("<int:id>/edit/", edit_TodoList, name= "todo_list_update"),
    path("<int:id>/delete/", delete_TodoList, name= "todo_list_delete"),
    path('items/create/', create_TodoItem, name="todo_item_create"), 
    path('items/<int:id>/edit/', update_TodoItem, name="todo_item_update"),
]
